
/**
 *
 */
import java.util.*;
import java.util.regex.*;

/**
 * @author Minglun Zhang
 *
 */
public class KeyWord {

    /**
     * @param args
     */
    public static void main ( String[] args ) {
        // 0: stopwords reading, 1: titles reading
        int mode = 0;
        ArrayList<String> titles = new ArrayList<String>();
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<String> stopWords = new ArrayList<String>();
        if ( args.length > 0 ) {
            throw new IllegalArgumentException( "Error: No args required" );
        }
        Pattern my_pattern = Pattern.compile( "[^a-zA-Z ]" );

        @SuppressWarnings ( "resource" )
        Scanner sc = new Scanner( System.in );
        String line;
        // read from input
        while ( sc.hasNextLine() ) {
            line = sc.nextLine();
            Matcher my_match = my_pattern.matcher( line );
            boolean check = my_match.find();
            if ( line.equals( "::" ) ) {
                mode = 1;
            }
            else if ( check ) {
                throw new IllegalArgumentException( "Error: no charactoers other than a-z, A-Z and white space" );
            }
            else if ( mode == 0 ) {
                if ( line.length() > 10 ) {
                    throw new IllegalArgumentException( "Error: no more than 10 characters in a word" );
                }

                stopWords.add( line.toLowerCase() );
                if ( stopWords.size() > 50 ) {
                    throw new IllegalArgumentException( "Error: no more than 50 words to ignore" );
                }
            }
            else {
                String[] arr = line.split( " " );
                if ( arr.length > 15 ) {
                    throw new IllegalArgumentException( "Error: no title contains more than 15 words" );
                }
                titles.add( line.toLowerCase() );
                if ( titles.size() > 200 ) {
                    throw new IllegalArgumentException( "Error: no more than 200 titles" );
                }
            }
        }
        sc.close();

        // create KWIC
        for ( int i = 0; i < titles.size(); i++ ) {
            String[] arr = titles.get( i ).split( " " );
            for ( int j = 0; j < arr.length; j++ ) {
                if ( !words.contains( arr[j] ) ) {
                    words.add( arr[j] );
                }
            }
        }

        // remove stopwords
        for ( int i = 0; i < stopWords.size(); i++ ) {
            words.remove( stopWords.get( i ) );
        }

        // sort KWIC from a - z
        Collections.sort( words );

        // print
        for ( int i = 0; i < words.size(); i++ ) {
            for ( int j = 0; j < titles.size(); j++ ) {
                String[] printable = titles.get( j ).split( " " );

                int idx = 0;
                while ( idx < printable.length ) {
                    if ( printable[idx].equals( words.get( i ) ) ) {
                        for ( int k = 0; k < printable.length; k++ ) {
                            if ( k == idx ) {
                                System.out.print( printable[k].toUpperCase() );
                            }
                            else {
                                System.out.print( printable[k] );
                            }
                            if ( k != printable.length - 1 ) {
                                System.out.print( " " );
                            }
                        }
                        System.out.println();
                    }
                    idx++;
                }

            }
        }
    }

}
