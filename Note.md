**A Philosophy of Software Design**

- How to divide a complicated problem into pieces is important.
- Comments should be describe things that are not obvious from the code.
- Common mistakes of creating small methods, classes. (e.g. No more than N lines)
- Define semantics to eliminate exceptions
- Minimize the number of places where exceptions must be handled.




**A Panel Discussion on #NoEstimates**

- Deadline can be changed, if there is a good reason.
- A messy burn up chart can be really useful to show what you think
- If the team can deliver consistently sprint over the sprint, the creation of estimates can move be performed by the stakeholders.
- Exploring how to meet needs requires soft skills, trust, and good relationships to succeed.



